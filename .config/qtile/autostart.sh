#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

killall -9 xsettingsd dunst xfce4-power-manager volumeicon xfce4-session 

lxsession &
dbus-launch xfce4-session &
nm-applet &
blueman-applet &
flameshot &
timedatectl set-timezone America/Guayaquil &
/home/jackson/.config/conky/Regulus/start.sh &
#cbatticon -u 5 &
gnome-keyring-daemon &
feh --bg-scale --randomize ~/.config/qtile/wallpapers/* &
#touchegg &
#python3 ~/.config/qtile/wallpapers/random_wallpaper.py &
picom --config $HOME/.config/qtile/scripts/picom.conf  &
udiskie &
#xsettingsd &

if [[!`pidof xfce-polkit`]]; then
  /usr/lib/xfce-polkit/xfce-polkit
fi

dunst -conf ~/.config/qtile/scripts/dunstrc &
lxqt-powermanagement &
# xfce4-power-manager &
betterlockscreen -u /home/jackson/.config/qtile/wallpapers &
xscreensaver -no-splash &
volumeicon &
